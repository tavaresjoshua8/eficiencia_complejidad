import java.util.Random;

public class App {
    private static final int ARR_SIZE = 1000;

    private static final int MIN_VALUE = 100;
    private static final int MAX_VALUE = 1000;

    public static void main(String[] args) throws Exception {
        int arr[] = generateRandomArray(ARR_SIZE);

        // Probar la eficiencia del algoritmo de buscar mayor
        long startTime = System.nanoTime();
        int mayor = buscarMayor(arr);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("El mayor es: " + mayor);
        System.out.println("El tiempo de ejecucion fue: " + duration + " nanosegundos");
    }

    public static int[] generateRandomArray(int n) {
        int[] arr = new int[n];
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            arr[i] = r.nextInt(MAX_VALUE - MIN_VALUE+1);
        }
        return arr;
    }

    public static int buscarMayor(int[] arr) {
        // Asignar a la variable mayor el minimo posible
        int mayor = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            // Si algun dato del arreglo supera al mayor, se reemplaza
            if (arr[i] > mayor)
                mayor = arr[i];
        }
        return mayor;
    }
}
