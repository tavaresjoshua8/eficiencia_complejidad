## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `bin`: the folder to maintain compiled classes